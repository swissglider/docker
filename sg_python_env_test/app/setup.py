"""A setuptools based setup module."""

# from distutils.core import setup
import os.path
from setuptools import setup

INSTALL_REQUIRES = []
TEST_REQUIRES = []

REQ_PATH = 'requirements.txt'
REQ_PLUGIN_PATH = 'plugin_packages.txt'
REQ_TEST_PATH = 'test_requirements.txt'

if os.path.isfile(REQ_PATH):
    with open(REQ_PATH) as fp1:
        INSTALL_REQUIRES = fp1.read()

if os.path.isfile(REQ_PLUGIN_PATH):
    with open(REQ_PLUGIN_PATH) as fp2:
        TEST_REQUIRES = fp2.read()

if os.path.isfile(REQ_TEST_PATH):
    with open(REQ_TEST_PATH) as fp3:
        TEST_REQUIRES += fp3.read()

setup(
    name='sg_pytest',
    version='1.0',
    package_dir={'': 'src/'},
    py_modules=["sg_pytest"],
    packages=["sg_pytest"],
    setup_requires=["pytest-runner"],
    zip_safe=False,
    install_requires=INSTALL_REQUIRES,
    tests_require=TEST_REQUIRES,
    # tests_require=['pytest', 'pytest-cov', 'pytest-html'],
    entry_points={
        'console_scripts': ['sg_flask=sg_pytest.webapp.main:main']
    }
)
