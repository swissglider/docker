#!/usr/bin/env bash
# Usage
# sg_pytest.sh test_run_dir pytest_parameters
# Sample 
# sh sg_pytest.sh /test_run /test_run/src /test_run/testing --rootdir=/test_run --html=/static/test.html --flake8 --codestyle
# sh sg_pytest.sh /test_run /test_run/src /test_run/testing --cov=/test_run/src --cov-report=html:/static/cov

# Read the input parameter
test_run_dir=$1
shift
pytest_parameters=$@

# activate virtual env
if [ -d "$test_run_dir" ]
then
    cd $test_run_dir
    echo $pytest_parameters
    output=$(pytest $pytest_parameters)
    # pytest $pytest_parameters
    exitcode=$?
else
    echo Can not find test_run_dir: $test_run_dir >&2
    exitcode=99
fi
exit $exitcode