# pylint: disable=redefined-outer-name,duplicate-code
"""Handle Route_Pytest tests."""

import json
from contextlib import contextmanager
import pytest
from flask import template_rendered
from sg_pytest.sg_pytest import SGPytest
from sg_pytest.webapp.main import app
from sg_pytest import sg_const


@pytest.fixture(scope='module')
def flask_app():
    """Handle flask app."""
    flask_app = app
    flask_app.config['TESTING'] = True
    return flask_app


@contextmanager
def captured_templates(app):
    """Capture template."""
    recorded = []

    # pylint: disable=unused-argument
    def record(sender, template, context, **extra):
        recorded.append((template, context))
    # pylint: enable=unused-argument

    template_rendered.connect(record, app)
    try:
        yield recorded
    finally:
        template_rendered.disconnect(record, app)


def test__sg_pytest_html(flask_app):
    """Test all ways."""
    test_client = flask_app.test_client()
    with captured_templates(flask_app) as templates:
        responce_object = test_client.get('/pytest/sg_pytest')
        assert responce_object.status_code == 200
        template, context = templates.pop()
        assert template.name == 'sg_pytest.html'
        # print(template.__dict__)
        assert context['title'] == 'SG PyTest'
        assert 'navigation' in context
        assert 'sg_pytest' in context
        assert context['session']['serial'] ==\
            context['sg_pytest'].serial
        assert context['session']['status'] ==\
            context['status'] == context['stati'][0]
        serial_1 = context['session']['serial']

    with captured_templates(flask_app) as templates:
        responce_object = test_client.get('/pytest/sg_pytest')
        assert responce_object.status_code == 200
        # assert len(templates) == 1
        template, context = templates.pop()
        assert template.name == 'sg_pytest.html'
        assert context['title'] == 'SG PyTest'
        assert 'navigation' in context
        assert 'sg_pytest' in context
        assert context['status'] == context['stati'][0]
        assert context['session']['serial'] ==\
            context['sg_pytest'].serial == serial_1
        assert context['session']['status'] ==\
            context['status'] == context['stati'][0]

    with captured_templates(flask_app) as templates:
        SGPytest.delete(serial_1)
        responce_object = test_client.get(
            '/pytest/sg_pytest',
            follow_redirects=True
        )
        assert responce_object.status_code == 200
        template, context = templates.pop()
        assert template.name == 'sg_pytest.html'
        assert context['session']['serial'] ==\
            context['sg_pytest'].serial
        assert serial_1 != context['sg_pytest'].serial != serial_1


def test_start_sg_pytest_html_1(flask_app):
    """Test Get with new session. It should forward."""
    client = flask_app.test_client()
    responce_object = client.get('/pytest/start_sg_pytest')
    assert responce_object.status_code == 302


def test_start_sg_pytest_html_2(flask_app):
    """Test Get with new session.

    It should forward and generate a new session.
    Than call again, and it should work

    """
    client = flask_app.test_client()
    with captured_templates(flask_app) as templates:
        responce_object = client.get(
            '/pytest/start_sg_pytest',
            follow_redirects=True
        )
        assert responce_object.status_code == 200
        template, context = templates.pop()
        assert template.name == 'sg_pytest.html'
        serial_1 = context['session']['serial']
        assert serial_1 == context['sg_pytest'].serial
    with captured_templates(flask_app) as templates:
        responce_object = client.get('/pytest/start_sg_pytest')
        assert responce_object.status_code == 200
        template, context = templates.pop()
        assert template.name == 'sg_pytest.html'
        assert serial_1 == context['session']['serial'] ==\
            context['sg_pytest'].serial
        assert context['title'] == 'SG PyTest'
        assert 'navigation' in context
        assert context['session']['status'] ==\
            context['status'] == context['stati'][1]
        assert len(context['status_array']) == 1
        assert context['stati'][1] in context['status_array']
        status_array = context['status_array'][context['stati'][1]]
        assert status_array['success_full']
        assert status_array['command'] == status_array['std_out']\
            == status_array['std_err'] == ['']
        assert status_array['exit_code'][0] == 0
        assert status_array['log_msg'][0] == "Start processing SG_Pytest"
        assert status_array['params']['plugins'] == []
        assert status_array['params']['src_dir'] == 'src'
        assert status_array['params']['test_dir'] == 'testing'


def test_start_sg_pytest_html_3(flask_app):
    """Test Pop with new session and advanced paramaeter.

    But without new_session parameter

    """
    client = flask_app.test_client()
    with captured_templates(flask_app) as templates:
        responce_object = client.get('/pytest/sg_pytest')
        assert responce_object.status_code == 200
        template, context = templates.pop()
        serial_1 = context['session']['serial']
    with captured_templates(flask_app) as templates:
        data = {
            'advanced_params': json.dumps({'test': 'hallo'})
        }
        responce_object = client.post(
            '/pytest/start_sg_pytest',
            data=data,
            content_type='multipart/form-data'
        )
        assert responce_object.status_code == 200
        template, context = templates.pop()
        assert template.name == 'sg_pytest.html'
        assert serial_1 == context['session']['serial']


def test_start_sg_pytest_html_4(flask_app):
    """Test Pop with new session and advanced paramaeter - new_session.

    It should forward and generate a new session.

    """
    client = flask_app.test_client()
    with captured_templates(flask_app) as templates:
        responce_object = client.get('/pytest/sg_pytest')
        assert responce_object.status_code == 200
    with captured_templates(flask_app) as templates:
        data = {
            'advanced_params': json.dumps({'new_session': True})
        }
        responce_object = client.post(
            '/pytest/start_sg_pytest',
            data=data,
            content_type='multipart/form-data'
        )
        assert responce_object.status_code == 302

    client = flask_app.test_client()
    with captured_templates(flask_app) as templates:
        responce_object = client.get('/pytest/sg_pytest')
        assert responce_object.status_code == 200
        context = templates.pop()[1]
        serial_1 = context['session']['serial']
    with captured_templates(flask_app) as templates:
        data = {
            'advanced_params': json.dumps({'new_session': True})
        }
        responce_object = client.post(
            '/pytest/start_sg_pytest',
            data=data,
            content_type='multipart/form-data',
            follow_redirects=True
        )
        assert responce_object.status_code == 200
        context = templates.pop()[1]
        assert serial_1 != context['session']['serial']


def test_start_sg_pytest_html_5(flask_app):
    """Test Pop with new session and empty params."""
    client = flask_app.test_client()
    with captured_templates(flask_app) as templates:
        responce_object = client.get('/pytest/sg_pytest')
    with captured_templates(flask_app) as templates:
        data = {
            'params': json.dumps({'new_session': True})
        }
        responce_object = client.post(
            '/pytest/start_sg_pytest',
            data=data,
            content_type='multipart/form-data'
        )
        assert responce_object.status_code == 200
        context = templates.pop()[1]
        status_array = context['status_array'][context['stati'][1]]
        assert status_array['params']['plugins'] == []
        assert status_array['params']['src_dir'] == 'src'
        assert status_array['params']['test_dir'] == 'testing'


def test_start_sg_pytest_html_6(flask_app):
    """Test Pop with new session and new params."""
    client = flask_app.test_client()
    with captured_templates(flask_app) as templates:
        responce_object = client.get('/pytest/sg_pytest')
    with captured_templates(flask_app) as templates:
        data = {
            'params': json.dumps({
                'plugins': ['hallo', 'Velo'],
                'src_dir': 'src_dir_velo',
                'test_dir': 'testing_hallo'
            })
        }
        responce_object = client.post(
            '/pytest/start_sg_pytest',
            data=data,
            content_type='multipart/form-data'
        )
        assert responce_object.status_code == 200
        context = templates.pop()[1]
        status_array = context['status_array'][context['stati'][1]]
        assert status_array['params']['plugins'] == ['hallo', 'Velo']
        assert status_array['params']['src_dir'] == 'src_dir_velo'
        assert status_array['params']['test_dir'] == 'testing_hallo'


def test_proceed_sg_pytest_1(flask_app):
    """Test without session, it return error."""
    client = flask_app.test_client()
    responce_object = client.get('/pytest/proceed_sg_pytest')
    assert responce_object.status_code == 200
    data = json.loads(responce_object.data.decode('UTF-8'))
    assert data['status'] == 'error'
    assert data['status_array']['error']['Error'] == 'Unknown Error'


def test_proceed_sg_pytest_2(flask_app):
    """Test with wrong session, it return error."""
    client = flask_app.test_client()
    responce_object = client.get('/pytest/sg_pytest')
    assert responce_object.status_code == 200
    responce_object = client.get('/pytest/proceed_sg_pytest')
    assert responce_object.status_code == 200
    data = json.loads(responce_object.data.decode('UTF-8'))
    assert data['status'] == 'error'
    assert data['status_array']['error']['Error'] ==\
        'Wrong or Unknown Status'


def test_proceed_sg_pytest_3(flask_app):
    """Test all ok."""
    org_script_1 = sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV
    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV =\
        '/testing/script/sg_create_test_env.sh'
    org_script_2 = sg_const.SHELL_SCRIPT_SG_PYTEST
    sg_const.SHELL_SCRIPT_SG_PYTEST = '/testing/script/sg_pytest.sh'
    client = flask_app.test_client()

    responce_object = client.get('/pytest/sg_pytest')
    assert responce_object.status_code == 200

    responce_object = client.get('/pytest/start_sg_pytest')
    data = {'params': json.dumps({'plugins': ['hallo', 'Velo']})}
    responce_object = client.post(
        '/pytest/start_sg_pytest',
        data=data,
        content_type='multipart/form-data'
    )
    assert responce_object.status_code == 200

    responce_object = client.get('/pytest/proceed_sg_pytest')
    assert responce_object.status_code == 200
    data = json.loads(responce_object.data.decode('UTF-8'))
    assert data['status'] == "2) Create the Test Environment finished"
    assert len(data['status_array']) == 2
    last_result =\
        data['status_array']["2) Create the Test Environment finished"]
    assert last_result["params"]["plugins"] == ['hallo', 'Velo']
    assert last_result['success_full']

    responce_object = client.get('/pytest/proceed_sg_pytest')
    assert responce_object.status_code == 200
    data = json.loads(responce_object.data.decode('UTF-8'))
    assert len(data['status_array']) == 3
    assert data['status'] == "finished"
    last_result = data['status_array']["finished"]
    assert last_result["params"]["plugins"] == ['hallo', 'Velo']
    assert last_result['success_full']

    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV = org_script_1
    sg_const.SHELL_SCRIPT_SG_PYTEST = org_script_2


def test_proceed_sg_pytest_4(flask_app):
    """Test all not successfull."""
    org_script_1 = sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV
    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV = '/testing/script/error'
    org_script_2 = sg_const.SHELL_SCRIPT_SG_PYTEST
    sg_const.SHELL_SCRIPT_SG_PYTEST = '/testing/script/sg_pytest.sh'
    client = flask_app.test_client()

    responce_object = client.get('/pytest/sg_pytest')
    assert responce_object.status_code == 200

    responce_object = client.get('/pytest/start_sg_pytest')
    data = {'params': json.dumps({'plugins': ['hallo', 'Velo']})}
    responce_object = client.post(
        '/pytest/start_sg_pytest',
        data=data,
        content_type='multipart/form-data'
    )
    assert responce_object.status_code == 200

    responce_object = client.get('/pytest/proceed_sg_pytest')
    assert responce_object.status_code == 200
    data = json.loads(responce_object.data.decode('UTF-8'))
    assert len(data['status_array']) == 2
    assert data['status'] == "error"
    last_result = data['status_array']["error"]
    assert not last_result['success_full']

    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV = org_script_1
    sg_const.SHELL_SCRIPT_SG_PYTEST = org_script_2


def test_proceed_sg_pytest_5(flask_app):
    """Test all ok."""
    org_script_1 = sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV
    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV =\
        '/testing/script/sg_create_test_env.sh'
    org_script_2 = sg_const.SHELL_SCRIPT_SG_PYTEST
    sg_const.SHELL_SCRIPT_SG_PYTEST = '/testing/script/error.sh'
    client = flask_app.test_client()

    responce_object = client.get('/pytest/sg_pytest')
    assert responce_object.status_code == 200

    responce_object = client.get('/pytest/start_sg_pytest')
    data = {'params': json.dumps({'plugins': ['hallo', 'Velo']})}
    responce_object = client.post(
        '/pytest/start_sg_pytest',
        data=data,
        content_type='multipart/form-data'
    )
    assert responce_object.status_code == 200

    responce_object = client.get('/pytest/proceed_sg_pytest')
    assert responce_object.status_code == 200

    responce_object = client.get('/pytest/proceed_sg_pytest')
    assert responce_object.status_code == 200
    data = json.loads(responce_object.data.decode('UTF-8'))
    assert len(data['status_array']) == 3
    assert data['status'] == "error"
    last_result = data['status_array']["error"]
    assert not last_result['success_full']

    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV = org_script_1
    sg_const.SHELL_SCRIPT_SG_PYTEST = org_script_2


def test_finished_sg_pytest_1(flask_app):
    """Test without session."""
    client = flask_app.test_client()
    responce_object = client.get('/pytest/finished_sg_pytest')
    assert responce_object.status_code == 302


def test_finished_sg_pytest_2(flask_app):
    """Test without status_array."""
    client = flask_app.test_client()
    responce_object = client.get('/pytest/sg_pytest')
    assert responce_object.status_code == 200
    responce_object = client.get('/pytest/finished_sg_pytest')
    assert responce_object.status_code == 200


def test_finished_sg_pytest_3(flask_app):
    """Test without session."""
    client = flask_app.test_client()
    responce_object = client.get('/pytest/sg_pytest')
    assert responce_object.status_code == 200
    responce_object = client.get('/pytest/proceed_sg_pytest')
    assert responce_object.status_code == 200
    responce_object = client.get('/pytest/finished_sg_pytest')
    assert responce_object.status_code == 302


def test_finished_sg_pytest_4(flask_app):
    """Test with status_array."""
    org_script_1 = sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV
    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV =\
        '/testing/script/sg_create_test_env.sh'
    org_script_2 = sg_const.SHELL_SCRIPT_SG_PYTEST
    sg_const.SHELL_SCRIPT_SG_PYTEST =\
        '/testing/script/sg_pytest.sh'
    client = flask_app.test_client()
    responce_object = client.get('/pytest/sg_pytest')
    assert responce_object.status_code == 200
    responce_object = client.get('/pytest/start_sg_pytest')
    data = {'params': json.dumps({
        'plugins': ['hallo', 'Velo']
        })}
    responce_object = client.post(
        '/pytest/start_sg_pytest',
        data=data,
        content_type='multipart/form-data'
    )
    assert responce_object.status_code == 200
    responce_object = client.get('/pytest/proceed_sg_pytest')
    assert responce_object.status_code == 200
    responce_object = client.get('/pytest/proceed_sg_pytest')
    assert responce_object.status_code == 200
    with captured_templates(flask_app) as templates:
        responce_object = client.get('/pytest/finished_sg_pytest')
        assert responce_object.status_code == 200
        context = templates.pop()[1]
        # print(json.dumps(context['session'], indent=4))
        assert 'status_array' not in context['session']
        assert context['session']['status'] ==\
            context['stati']['finished']

    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV = org_script_1
    sg_const.SHELL_SCRIPT_SG_PYTEST = org_script_2


def test_finished_sg_pytest_5(flask_app):
    """Test with status_array."""
    org_script_1 = sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV
    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV =\
        '/testing/script/sg_create_test_env.sh'
    org_script_2 = sg_const.SHELL_SCRIPT_SG_PYTEST
    sg_const.SHELL_SCRIPT_SG_PYTEST =\
        '/testing/script/sg_pytest.sh'
    client = flask_app.test_client()
    with captured_templates(flask_app) as templates:
        responce_object = client.get('/pytest/sg_pytest')
        assert responce_object.status_code == 200
        context = templates.pop()[1]
        assert 'sg_pytest' in context
        serial_1 = context['sg_pytest'].serial
    responce_object = client.get('/pytest/start_sg_pytest')
    data = {'params': json.dumps({'plugins': ['hallo', 'Velo']})}
    responce_object = client.post(
        '/pytest/start_sg_pytest',
        data=data,
        content_type='multipart/form-data'
    )
    assert responce_object.status_code == 200
    responce_object = client.get('/pytest/proceed_sg_pytest')
    assert responce_object.status_code == 200
    responce_object = client.get('/pytest/proceed_sg_pytest')
    assert responce_object.status_code == 200
    SGPytest.delete(serial_1)
    responce_object = client.get('/pytest/finished_sg_pytest')
    assert responce_object.status_code == 302

    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV = org_script_1
    sg_const.SHELL_SCRIPT_SG_PYTEST = org_script_2


def test_home_page(flask_app):
    """Test root page."""
    test_client = flask_app.test_client()
    response = test_client.get('/')
    assert response.status_code == 200


def test_index(flask_app):
    """Test index.html."""
    test_client = flask_app.test_client()
    response = test_client.get('/static/index.html')
    assert response.status_code == 404


def test_favicon(flask_app):
    """Test Favicon.ico."""
    test_client = flask_app.test_client()
    response = test_client.get('/static/favicon.ico')
    assert response.status_code == 200


def test_favicon1(flask_app):
    """Test Favicon.ico."""
    test_client = flask_app.test_client()
    response = test_client.get('/favicon.ico')
    assert response.status_code == 200


def test_wtf1(flask_app):
    """Test unknown page."""
    test_client = flask_app.test_client()
    response = test_client.get('/wtf.html')
    assert response.status_code == 302
