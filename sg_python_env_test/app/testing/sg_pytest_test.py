# pylint: disable=duplicate-code
"""The sg_pytest test script."""

import os
import shutil
from sg_pytest.sg_pytest import SGPytest
from sg_pytest import sg_const


def test_get_sg_pytest():
    """Test get_sg_pytest without input."""
    sg_pytest = SGPytest()

    assert sg_pytest.serial != ""
    assert sg_pytest.processing is False
    assert sg_pytest.status == 'initialized'
    assert sg_pytest.last_return_struct == {}
    assert sg_pytest.params['plugins'].sort() == [].sort()
    assert sg_pytest.params['src_dir'] == "src"
    assert sg_pytest.params['test_dir'] == "testing"
    assert sg_pytest.test_run_dir ==\
        sg_const.TEST_RUN_DIRECTORY + '/' + sg_pytest.serial
    assert sg_pytest.pytest_result_web_link ==\
        sg_const.STATIC_WEB_DIR + "/" + sg_pytest.serial + "/" +\
        sg_const.PYTEST_WEB_SITE
    assert sg_pytest.pytest_cov_result_web_link ==\
        sg_const.STATIC_WEB_DIR + "/" + sg_pytest.serial +\
        sg_const.PYTEST_COV_WEB_DIR + "/index.html"
    assert sg_pytest.static_dir ==\
        sg_const.STATIC_DIRECTORY + "/" + sg_pytest.serial


def test_get_sg_pytest1():
    """Test get_sg_pytest without input but take sg_pytest.ini."""
    input_param = {
        'use_sg_pytest_ini': True
    }
    sg_pytest = SGPytest()
    sg_pytest.update_parameter(input_param)

    assert sg_pytest.params['plugins'].sort() ==\
        ['--pep8', '--pylint'].sort()
    assert sg_pytest.params['use_sg_pytest_ini']


def test_get_sg_pytest2():
    """Test get_sg_pytest without input.

    But take sg_pytest.ini and do overwrite ini.

    """
    input_param = {
        'plugins': ['--pep8', '--pep257'],
        'src_dir': 'source',
        'test_dir': 'test'
    }
    sg_pytest = SGPytest()
    sg_pytest.update_parameter(input_param, use_sg_pytest_ini=True)

    assert sg_pytest.params['test_dir'] == 'test'
    assert sg_pytest.params['src_dir'] == 'source'
    assert sg_pytest.params['plugins'].sort() ==\
        ['--pylint', '--pep8', '--pep257'].sort()


def test_get_sg_pytest3():
    """Test get_sg_pytest without input.

    But take sg_pytest.ini and do not overwrite ini.

    """
    input_param = {
        'plugins': ['--pep8', '--pep257'],
        'src_dir': 'source',
        'test_dir': 'test'
    }
    sg_pytest = SGPytest()
    sg_pytest.update_parameter(
        input_param,
        use_sg_pytest_ini=True,
        overwrite_sg_pytest_ini=False
    )

    assert sg_pytest.params['test_dir'] == 'testing'
    assert sg_pytest.params['src_dir'] == 'src'
    assert sg_pytest.params['plugins'].sort() ==\
        ['--pylint', '--pep8', '--pep257'].sort()


def test_get_sg_pytest4():
    """Test get_sg_pytest.

    While calling get_sg_pytest 3 times and compare the serial

    """
    sg_pytest = SGPytest()
    serial_1 = sg_pytest.serial
    sg_pytest = SGPytest()
    serial_2 = sg_pytest.serial
    del sg_pytest
    sg_pytest = SGPytest()
    serial_3 = sg_pytest.serial

    assert serial_1 != serial_2
    assert serial_1 != serial_3
    assert serial_2 != serial_3


def test_get_sg_pytest5():
    """Test get_sg_pytest.

    While calling get_sg_pytest 3 times with params and compare the serial

    """
    sg_pytest = SGPytest()
    serial_1 = sg_pytest.serial
    params_1 = sg_pytest.params
    sg_pytest = SGPytest(serial_1)
    serial_2 = sg_pytest.serial
    params_2 = sg_pytest.params
    del sg_pytest
    sg_pytest = SGPytest(serial_1)
    serial_3 = sg_pytest.serial
    params_3 = sg_pytest.params

    assert serial_1 == serial_2
    assert serial_1 == serial_3
    assert serial_2 == serial_3

    assert params_1 == params_2
    assert params_1 == params_3
    assert params_2 == params_3


def test_get_sg_pytest6():
    """Test get_sg_pytest.

    While calling get_sg_pytest 3 times with params put different parameter

    """
    params = {}
    sg_pytest = SGPytest()
    sg_pytest.update_parameter(params)
    serial_1 = sg_pytest.serial
    new_params_1 = sg_pytest.params
    del sg_pytest

    params = new_params_1.copy()
    params['plugins'] = ['--flake8']
    sg_pytest = SGPytest(serial_1)
    sg_pytest.update_parameter(params)
    serial_2 = sg_pytest.serial
    new_params_2 = sg_pytest.params.copy()

    assert new_params_1 != new_params_2
    assert params != new_params_1
    assert params == new_params_2

    assert serial_1 == serial_2
    assert new_params_1['plugins'] != new_params_2['plugins']
    assert new_params_1['src_dir'] == new_params_2['src_dir']
    assert new_params_1['test_dir'] == new_params_2['test_dir']


def test_last_return_struct1():
    """Test lat return struct."""
    sg_pytest = SGPytest()
    serial_1 = sg_pytest.serial

    last_return_struct_1 = sg_pytest.last_return_struct
    del sg_pytest
    sg_pytest = SGPytest(serial_1)
    last_return_struct_2 = sg_pytest.last_return_struct
    assert last_return_struct_1 == last_return_struct_2

    retrun_struct_1 = sg_pytest.start_processing_test()
    del sg_pytest

    sg_pytest = SGPytest(serial_1)
    last_return_struct_2 = sg_pytest.last_return_struct
    del sg_pytest
    assert retrun_struct_1 == last_return_struct_2


def test_start_processing1():
    """Simple start start_processing."""
    sg_pytest = SGPytest()
    retrun_struct = sg_pytest.start_processing_test()

    assert retrun_struct['success_full']
    assert retrun_struct['log_msg'][0] == "Start processing SG_Pytest"
    assert sg_pytest.status == "Start processing SG_Pytest - finished"


def test_create_the_test_env1():
    """Simple test create_the_test_env."""
    org_script = sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV
    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV =\
        '/testing/script/sg_create_test_env.sh'

    sg_pytest = SGPytest()
    serial_1 = sg_pytest.serial
    retrun_struct_1 = sg_pytest.create_the_test_env()
    del sg_pytest

    sg_pytest = SGPytest(serial_1)
    retrun_struct_2 = sg_pytest.create_the_test_env(True)
    del sg_pytest

    sg_pytest = SGPytest(serial_1)
    retrun_struct_3 = sg_pytest.create_the_test_env(use_new_env=False)

    assert 'Create Test Environment (True)' in\
        retrun_struct_1['log_msg'][0]
    assert 'Create Test Environment (True)' in\
        retrun_struct_2['log_msg'][0]
    assert 'Create Test Environment (False)' in\
        retrun_struct_3['log_msg'][0]

    del sg_pytest
    SGPytest.delete(serial=serial_1)
    sg_pytest = SGPytest(serial_1)
    serial_2 = sg_pytest.serial

    assert serial_1 != serial_2

    retrun_struct_4 = sg_pytest.create_the_test_env()
    assert 'Create Test Environment (True)'\
        in retrun_struct_4['log_msg'][0]
    assert os.path.isdir(sg_pytest.test_run_dir)

    if os.path.isdir(sg_const.TEST_RUN_DIRECTORY + "/" + serial_2):
        shutil.rmtree(sg_const.TEST_RUN_DIRECTORY + "/" + serial_2)
    assert not os.path.isdir(sg_pytest.test_run_dir)

    return_struct_5 = sg_pytest.create_the_test_env(use_new_env=False)
    assert 'Create Test Environment (True)'\
        in return_struct_5['log_msg'][0]
    del sg_pytest

    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV = org_script


def test_execute_pytest1():
    """Simple test execute_pytest."""
    org_script_1 = sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV
    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV =\
        '/testing/script/sg_create_test_env.sh'
    org_script_2 = sg_const.SHELL_SCRIPT_SG_PYTEST
    sg_const.SHELL_SCRIPT_SG_PYTEST = '/testing/script/sg_pytest.sh'

    sg_pytest = SGPytest()
    serial_1 = sg_pytest.serial
    retrun_struct_1 = sg_pytest.execute_pytest()
    assert not retrun_struct_1['success_full']
    assert retrun_struct_1['exit_code'][0] == 99
    assert "Can not find test_run_dir" in retrun_struct_1['std_err'][0]

    sg_pytest.create_the_test_env()
    retrun_struct_2 = sg_pytest.execute_pytest()
    assert retrun_struct_2['success_full']
    assert retrun_struct_2['exit_code'][0] == 1
    assert "Run the sg_pytest - Result: Exit code 1:"\
        in retrun_struct_2['log_msg'][0]

    if not os.path.isdir(sg_pytest.static_dir):
        os.makedirs(sg_pytest.static_dir)
    SGPytest.delete(serial_1)
    del sg_pytest

    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV = org_script_1
    sg_const.SHELL_SCRIPT_SG_PYTEST = org_script_2


def test_delete():
    """Simple test delete."""
    org_script_1 = sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV
    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV =\
        '/testing/script/sg_create_test_env.sh'
    org_script_2 = sg_const.SHELL_SCRIPT_SG_PYTEST
    sg_const.SHELL_SCRIPT_SG_PYTEST = '/testing/script/sg_pytest.sh'

    sg_pytest = SGPytest()
    serial_1 = sg_pytest.serial

    sg_pytest.create_the_test_env()
    sg_pytest.execute_pytest()
    pickle_file = sg_pytest._get_pickle_file(serial_1)  # pylint: disable=W
    if os.path.exists(pickle_file):
        os.remove(pickle_file)
    SGPytest.delete(serial_1)
    del sg_pytest

    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV = org_script_1
    sg_const.SHELL_SCRIPT_SG_PYTEST = org_script_2
