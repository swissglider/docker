#!/usr/bin/env bash
# Usage
# sg_create_virtual_env.sh.sh test_run_dir app_dir
# Sample 
# sh script/sg_create_virtual_env.sh /test_run /app

# Read the input parameter
# Read the input parameter
test_run_dir=$1
shift
app_dir=$1


# create virtual env
cd $test_run_dir

# install test and app packages
if [ -e plugin_packages.txt ]
then
    pip install --quiet --requirement plugin_packages.txt
fi
# delete all setup.py
find . -type f -name setup.py -exec rm -r {} \+

# delete all setup.cfg
find . -type f -name setup.cfg -exec rm -r {} \+

exit 0