#!/usr/bin/env bash
# Usage
# sg_pytest.sh test_run_dir pytest_parameters
# Sample 
# sh sg_pytest.sh /test_run /test_run/src /test_run/testing --rootdir=/test_run --html=/static/test.html --flake8 --codestyle
# sh sg_pytest.sh /test_run /test_run/src /test_run/testing --cov=/test_run/src --cov-report=html:/static/cov

# Read the input parameter
test_run_dir=$1
shift
pytest_parameters=$@

html_string="--html="
cov_string="--cov-report=html:"


for var in $pytest_parameters
do
    if [ "$var" != "${var%$html_string*}" ];
    then
        test_html_file=$(echo $var|sed -e 's/'"${html_string}"'//')
        test_html_dir=$(echo $test_html_file|sed -e 's/test.html//')
    elif [ "$var" != "${var%$cov_string*}" ];
    then
        cov_html_dir=$(echo $var|sed -e 's/'"${cov_string}"'//')
        cov_html_file="$cov_html_dir/index.html"
    fi
done

mkdir -p $test_html_dir
mkdir -p $cov_html_dir

echo "Hi I'm the test html in $test_html_file" > $test_html_file
echo "Hi I'm the conv html in $test_html_file" > $cov_html_file

if [ -d "$test_run_dir" ]
then
    cd $test_run_dir
    echo $pytest_parameters
    # output=$(pytest $pytest_parameters)
    # sleep 2s
    exitcode=1
else
    echo Can not find test_run_dir: $test_run_dir >&2
    exitcode=99
fi

exit $exitcode