#!/usr/bin/env bash
# Usage
# sg_create_test_env.sh data_dir test_run_dir
# Sample 
# sh script/sg_create_test_env.sh /data /test_run

# Read the input parameter
data_dir=$1
shift
test_run_dir=$1

# Delete test_run directory if still available
rm -rf $test_run_dir

# create test_run directory
mkdir -p $test_run_dir
# copy all files from data test_run dir
cp -R $data_dir/* $test_run_dir/
cd $test_run_dir

# # install test and app packages
# if [ -e plugin_packages.txt ]
# then
#     pip install --quiet --requirement plugin_packages.txt
# fi
# pip install --quiet --editable .

# delete all __pycache__
find . -type d -name __pycache__ -exec rm -r {} \+

# delete all *egg-info*
find . -type d -name *egg-info* -exec rm -r {} \+

# delete all .pytest_cache
find . -type d -name .pytest_cache -exec rm -r {} \+

# delete all .coverage
find . -type f -name .coverage -exec rm -r {} \+

# delete all pytest.ini
find . -type f -name pytest.ini -exec rm -r {} \+

# delete all tox.ini
find . -type f -name tox.ini -exec rm -r {} \+

# delete all setup.py
find . -type f -name setup.py -exec rm -r {} \+

# delete all setup.cfg
find . -type f -name setup.cfg -exec rm -r {} \+

# sleep 2s

exit 0
