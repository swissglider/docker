"""Const for the whole package."""

PLUGIN_LIST = [
    "--docstyle",
    "--pep8",
    "--pylint",
    "--flake8",
    "--codestyle",
    "--pep257"
]

TEST_RUN_DIRECTORY = "/test_run"
DATA_DIRECTORY = "/data"
APP_DIRECTORY = "/app"
STATIC_DIRECTORY = "/static"
PICKLE_SG_PYTEST_FILE = "/tmp/sg_pytest.pkl"
PYTEST_EXIT_CODE = {
    0: "Exit code 0:	All tests were collected and passed successfully",
    1: "Exit code 1:	Tests were collected and run but some tests failed",
    2: "Exit code 2:	Test execution was interrupted by the user",
    3: "Exit code 3:	Internal error happened while executing tests",
    4: "Exit code 4:	pytest command line usage error",
    5: "Exit code 5:	No tests were collected",
    99: "Exit code 99:	Script Error"
}
SG_PYTEST_INI = DATA_DIRECTORY + "/sg_pytest.ini"
DEFAULT_TEST_DIR = 'testing'
DEFAULT_SRC_DIR = 'src'
STATIC_WEB_DIR = "/static"
PYTEST_WEB_SITE = "test.html"
PYTEST_COV_WEB_DIR = "/cov"

REQ_PATH = 'requirements.txt'
REQ_PLUGIN_PATH = 'plugin_packages.txt'

SHELL_SCRIPT_SG_CREATE_TEST_ENV = 'script/sg_create_test_env.sh'
SHELL_SCRIPT_SG_PYTEST = 'script/sg_pytest.sh'
SHELL_SCRIPT_SG_CLEAN_TEST_ENV = 'script/sg_clean_test_env.sh'
