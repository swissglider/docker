# pylint: disable=cyclic-import
"""All SG_Pytest Requests."""
import json
from flask import render_template, request, session, redirect, jsonify
from sg_pytest.webapp.main import app
from sg_pytest.sg_pytest import SGPytest


navigation = [
    {
        'title': 'Home',
        'id': 'home',
        'link': '/index'
    },
    {
        'title': 'SG PyTest',
        'id': 'sg_pytest',
        'link': '/pytest/sg_pytest'
    },
    {
        'title': 'Test3',
        'id': 'test_3',
        'link': 'http://tagi.ch'
    }
]

processing_stati = {
    0: "0) New SGPytest request",
    1: "1) Proceeding SGPytest finished",
    2: "2) Create the Test Environment finished",
    'finished': "finished",
    'error': "error"

}


@app.route('/')
@app.route('/index')
def index():
    """Return index page."""
    return render_template('index.html', title='Home', navigation=navigation)


@app.route('/pytest/sg_pytest')
def sg_pytest_init():
    """Init SG_Pytest request."""
    if 'serial' not in session:
        session.clear()
        sg_pytest = SGPytest()
        sg_pytest.update_parameter(use_sg_pytest_ini=True)
        session['serial'] = sg_pytest.serial
    else:
        if not SGPytest.is_serial_available(session['serial']):
            return clear_sg_session()
        sg_pytest = SGPytest(session['serial'])
    return render_template(
        'sg_pytest.html',
        title='SG PyTest',
        navigation=navigation,
        sg_pytest=sg_pytest,
        status=set_status(0),
        stati=processing_stati
    )


@app.route('/pytest/start_sg_pytest', methods=['POST', 'GET'])
def start_sg_pytest():
    """Start SG_Pytest request."""
    if not check_sg_session():
        return clear_sg_session()

    if 'advanced_params' in request.form:
        tmp_ad_params = json.loads(request.form['advanced_params'])
        if tmp_ad_params and 'new_session' in tmp_ad_params\
                and tmp_ad_params['new_session']:
            return clear_sg_session()

    new_params = {}

    if 'params' in request.form:
        tmp_params = json.loads(request.form['params'])
        if 'src_dir' in tmp_params:
            new_params['src_dir'] = tmp_params['src_dir']
        if 'test_dir' in tmp_params:
            new_params['test_dir'] = tmp_params['test_dir']
        if 'plugins' in tmp_params:
            new_params['plugins'] = tmp_params['plugins']
    sg_pytest = SGPytest(session['serial'])
    sg_pytest.update_parameter(new_params)

    return_struct = sg_pytest.start_processing_test()
    return render_template(
        'sg_pytest.html',
        title='SG PyTest',
        navigation=navigation,
        sg_pytest=sg_pytest,
        status=set_status(1, return_struct),
        status_array=get_status_array(),
        stati=processing_stati
    )


@app.route('/pytest/finished_sg_pytest')
def finished_sg_pytest():
    """Finish sg_pytest request."""
    if not check_sg_session():
        return clear_sg_session()
    status_array = get_status_array()
    # reset session status_array
    if 'status_array' in session:
        del session['status_array']
    if not SGPytest.is_serial_available(session['serial']):
        return clear_sg_session()
    sg_pytest = SGPytest(serial=session['serial'])
    return render_template(
        'sg_pytest.html',
        title='SG PyTest',
        navigation=navigation,
        sg_pytest=sg_pytest,
        status=session['status'],
        status_array=status_array,
        stati=processing_stati
    )


@app.route('/pytest/proceed_sg_pytest')
def proceed_sg_pytest():
    """Proceed sg_pytest request."""
    if not check_sg_session()\
            or not SGPytest.is_serial_available(session['serial']):
        clear_sg_session()
        result = {
            'status': processing_stati['error'],
            'status_array': {'error': {'Error': 'Unknown Error'}}
        }
        return jsonify(result)

    sg_pytest = SGPytest(serial=session['serial'])
    status = ''
    status_array = {}

    if session['status'] == processing_stati[1]:
        return_struct = sg_pytest.create_the_test_env()
        if 'success_full' in return_struct and return_struct['success_full']:
            status = set_status(2, return_struct)
            status_array = session['status_array']
        else:
            status = set_status('error', return_struct)
            status_array = session['status_array']
            clear_sg_session()
    elif session['status'] == processing_stati[2]:
        return_struct = sg_pytest.execute_pytest()
        if 'success_full' in return_struct and return_struct['success_full']:
            status = set_status('finished', return_struct)
            status_array = session['status_array']
        else:
            status = set_status('error', return_struct)
            status_array = session['status_array']
            clear_sg_session()
    else:
        return_struct = {'Error': 'Wrong or Unknown Status'}
        status = set_status('error', return_struct)
        status_array = session['status_array']
        clear_sg_session()

    return jsonify({'status': status, 'status_array': status_array})


def check_sg_session():
    """Check if session and status ar set."""
    if not ('serial' in session and 'status' in session):
        return False
    return True


def clear_sg_session():
    """Clear session and SGPytest."""
    if 'serial' in session:
        SGPytest.delete(session['serial'])
    session.clear()
    return redirect('/pytest/sg_pytest')


def get_status_array() -> dict:
    """Return the status_array."""
    status_array = {}
    if 'status_array' in session:
        status_array = session['status_array']
    return status_array


def set_status(status, return_struct: dict = None) -> str:
    """Set the status and status_array."""
    session['status'] = processing_stati[status]

    if return_struct:
        status_array = get_status_array()
        status_array[session['status']] = return_struct
        session['status_array'] = status_array

    return session['status']
