function run_sg_pytest() {
    $("#status_container").hide();
    $("#error_container").hide();
    set_current_status("running ...")
    secInterval = setInterval(run_sg_pytest, 200);
    run_status = 'start'
    _status_array = {}
    _error_array = {}

    function run_sg_pytest() {
        if (run_status == 'running') {
            return
        } else if (run_status == 'start') {
            var params = {};
            params['plugins'] = []
            $('div#config_param input[type=checkbox]').each(function () {
                if ($(this).is(":checked")) {

                    params['plugins'].push('--' + $(this).attr('id'))
                }
            })
            params['src_dir'] = $("#src_dir").val()
            params['test_dir'] = $("#test_dir").val()
            run_status = 'running'
            set_current_status("Start processing sg_pytest...")
            url = '/sg_pytest/start_processing_test'
            run_function(url, "start_processing_test finished", 'failed',
                'start_processing_test', _status_array, _error_array, dat_st = params)
        } else if (run_status == 'start_processing_test finished') {
            run_status = 'running'
            set_current_status("Create the test Environment...")
            url = '/sg_pytest/create_the_test_env'
            run_function(url, "create_the_test_env finished", 'failed',
                'create_the_test_env', _status_array, _error_array)
        } else if (run_status == 'create_the_test_env finished') {
            run_status = 'running'
            set_current_status("Create the virtual Environment...")
            url = '/sg_pytest/create_the_virtual_env'
            run_function(url, "create_the_virtual_env finished", 'failed',
                'create_the_virtual_env', _status_array, _error_array)
        } else if (run_status == 'create_the_virtual_env finished') {
            run_status = 'running'
            set_current_status("Execute the sg_pythest...")
            url = '/sg_pytest/execute_pytest'
            run_function(url, "execute_pytest finished", 'failed', 'execute_pytest',
                _status_array, _error_array)
        } else if (run_status == 'execute_pytest finished' || run_status == 'failed') {
            run_status = 'running'
            set_current_status("Clean the test Environment...")
            url = '/sg_pytest/clean_test_env'
            run_function(url, "finished", 'failed', 'clean_test_env', _status_array,
                _error_array)
        } else if (run_status == 'finished') {
            set_current_status_to_finished("sg_pytest finished...")
            clearInterval(secInterval)
        } else {
            clearInterval(secInterval)
            set_current_status("Wrong run_status: " + run_status)
        }
    }
}

function run_function(url_to_call, good_status, bad_status, action, status_array, error_array, dat_st = {}) {
    $.ajax({
        url: url_to_call,
        type: "POST",
        data: JSON.stringify(dat_st),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            log_result(result, action, status_array)
            run_status = good_status
        },
        error: function (error) {
            log_error(error, action, error_array)
            run_status = bad_status
        }
    })
}

function set_current_status(status) {
    output = ""
    output += '<a href="#" class="close" data-dismiss="alert">&times;</a>'
    output +=
        '<i class="fas fa-hourglass-half"></i><strong> Current running Status!</strong> (wait till finished) <br>' +
        '<span class="blink_me">' + status + '</span>'
    document.getElementById("current_status").innerHTML = output
}

function set_current_status_to_finished(status) {
    output = ""
    output += '<a href="#" class="close" data-dismiss="alert">&times;</a>'
    output += '<i class="fas fa-check"></i><strong> Success!</strong> <br> ' + status
    document.getElementById("current_status").innerHTML = output
}

function log_result(result, action, status_array) {
    $("#status_container").show()
    status_array[action] = result
    $("#status").JSONView(status_array, {
        collapsed: false,
        nl2br: true,
        recursive_collapser: false
    })
    $('#status').JSONView('collapse', 2);
}

function log_error(error, action, error_array) {
    $("#error_container").show()
    error_array[action] = error
    $("#error").JSONView(error_array, {
        collapsed: true,
        nl2br: true,
        recursive_collapser: true
    })
    $('#error').JSONView('collapse', 2);
}

function blinker() {
    $('.blink_me').fadeOut(500);
    $('.blink_me').fadeIn(500);
}

setInterval(blinker, 1000);