function get_config_param(callback) {
    url_to_call = "/helper/get_config_param"
    $.ajax({
        url: url_to_call,
        success: function (result) {
            callback(result)
        },
        error: function (error) {
            log_error(error, action, error_array)
            run_status = bad_status
        }
    })
}