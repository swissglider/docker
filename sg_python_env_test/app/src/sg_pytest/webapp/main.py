# pylint: disable=cyclic-import
"""This is the webapp main."""

import os
from flask import Flask, send_file, redirect
from sg_pytest import sg_const

app = Flask(__name__)  # pylint: disable=C
app.config['SECRET_KEY'] = "And now the show goes on !!"

import sg_pytest.webapp.route_pytest  # noqa: F401,E402

# Everything not declared before (not a Flask route / API endpoint)...


@app.route('/<path:path>')
def route_frontend(path):
    """Route the frontend."""
    # ...could be a static file needed by the front end that
    # doesn't use the `static` path (like in `<script src="bundle.js">`)
    file_path = os.path.join(app.static_folder, path)
    if os.path.isfile(file_path):
        return send_file(file_path)
    # ...or should be handled by the SPA's "router" in front end
    return redirect('/')


def main():  # pragma: no cover
    """Handle the development."""
    # Only for debugging while developing
    sg_const.STATIC_DIRECTORY = "/data/src/sg_pytest/webapp/static/temp_static"
    sg_const.STATIC_WEB_DIR = "/static/temp_static"
    sg_const.SHELL_SCRIPT_SG_PYTEST = '/testing/script/sg_pytest.sh'
    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV = \
        '/testing/script/sg_create_test_env.sh'
    app.run(host='0.0.0.0', debug=True, port=80)
    # Guido


if __name__ == "__main__":  # pragma: no cover
    main()
    # # Only for debugging while developing
    # app.run(host='0.0.0.0', debug=True, port=80)
    # app.app_context()
    # # Guido
