"""Class SGPytest for run tests against Source Code and runs unittest."""

import pickle
import subprocess
import configparser
import uuid
import shutil
import os
from sg_pytest import sg_const


class SGPytest():  # pylint: disable=too-many-arguments,duplicate-code
    """Core SGPytest class that runs all tests.

    Note:
        All parameter from any config file
        are overwritten except sg_pytest.ini.

    Output:
        the output of the sg_pytests are html files:
        1) --> /static/test.html
        2) --> /static/cov/index.html

    Example 1:
        ```python
        from sg_pytest import SGPytest
        params = {
            "plugins":[
                'pep8',
                'pylint',
                'flake8'
            ]
            "src_dir":"src",
            "test_dir":"testing"

        }
        sg_pt = SGPytest.get_sg_pytest(params)
        if sg_pt:
            sg_pt.start_processing_test()
            sg_pt.create_the_test_env()
            sg_pt.execute_pytest()
            sg_pt.clean_test_env()
        else:
            print("This SG_PYTEST is currently processing")
        ```
    Example 2:
        ```python
        from sg_pytest import SGPytest
        # params from last request with serial
        sg_return_value = SGPytest.last_return_struct(serial)
        if not sg_return_value:
            print("This SG_PYTEST has no last_return_struct")
        else:
            print("Current sg_pytest status is: %s - %s"
                % (
                    sg_return_value['params']['status'],
                    str(sg_return_value['params']['processing']
                ))

    Return:
        Console output as String

    """

    def __init__(self, serial: str = None):
        """Init the SGPytest.

        Return SG_Pytest

        Args:
            serial  (str):          for existing else None
                                    Default = None
        """
        self._serial = ''
        if serial and SGPytest._is_pickle_file_available(serial):
            tmp = SGPytest._load(serial)
            self._serial = tmp.serial
            self._params = tmp.params
            self._last_return_struct = tmp.last_return_struct
            self._status = tmp.status
            self._processing = tmp.processing
            del tmp
        else:
            serial = str(uuid.uuid4())
            SGPytest._dump({}, serial)
            self._serial = serial
            self._params = {}
            self.update_parameter()
            self._last_return_struct = {}
            self._status = 'initialized'
            self._processing = False
        # check working directory and static directory
        self._test_run_dir = sg_const.TEST_RUN_DIRECTORY + "/" + self.serial
        static_web_dir = sg_const.STATIC_WEB_DIR + "/" + self.serial
        self._pytest_result_web_link = static_web_dir \
            + "/" + sg_const.PYTEST_WEB_SITE
        self._pytest_cov_result_web_link = static_web_dir \
            + sg_const.PYTEST_COV_WEB_DIR + "/index.html"
        self._static_dir = sg_const.STATIC_DIRECTORY + "/" + self.serial
        SGPytest._dump(self, serial)

    @staticmethod
    def delete(serial: str):
        """Delete the pickle and all related files and the object."""
        if serial:
            temp_sg = SGPytest(serial=serial)
            if temp_sg and temp_sg._params:
                if os.path.isdir(temp_sg.test_run_dir):
                    shutil.rmtree(temp_sg.test_run_dir)
                if os.path.isdir(temp_sg.static_dir):
                    shutil.rmtree(temp_sg.static_dir)
            pickle_file = SGPytest._get_pickle_file(serial)
            if os.path.exists(pickle_file):
                os.remove(pickle_file)
            if os.path.isdir(sg_const.TEST_RUN_DIRECTORY + "/" + serial):
                shutil.rmtree(sg_const.TEST_RUN_DIRECTORY + "/" + serial)
            if os.path.isdir(sg_const.STATIC_DIRECTORY + "/" + serial):
                shutil.rmtree(sg_const.STATIC_DIRECTORY + "/" + serial)
            if temp_sg:
                del temp_sg
            # temp_sg.stop_processing()

    @staticmethod
    def is_serial_available(serial: str) -> bool:
        """Check if serial available."""
        return SGPytest._is_pickle_file_available(serial)

    @staticmethod
    def _get_pickle_file(serial: str) -> str:
        """Return the pickle serialized file."""
        return sg_const.PICKLE_SG_PYTEST_FILE + "_" + serial

    @staticmethod
    def _is_pickle_file_available(serial: str) -> str:
        """Check if pickle file is available."""
        return os.path.exists(SGPytest._get_pickle_file(serial))

    @staticmethod
    def _load(serial: str = None):
        """Read the pickelt SG_PYTEST."""
        with open(SGPytest._get_pickle_file(serial), 'rb') as input:
            return pickle.load(input)

    @staticmethod
    def _dump(sg_pytest, serial):
        """Write the pickelt SG_PYTEST."""
        with open(SGPytest._get_pickle_file(serial), 'wb') as output:
            pickle.dump(sg_pytest, output, pickle.HIGHEST_PROTOCOL)

    def update_parameter(
            self,
            params: dict = None,
            overwrite_sg_pytest_ini: bool = True,
            use_sg_pytest_ini: bool = False
    ):
        """Set the SGPytest params.

        params   (dict):        dict with the with th following parameter:
                src_dir  (str):         source directory to test.
                                        Default = src
                test_dir (str):         testing directory to test.
                                        Default = testing
                plugins  (list):        list of plugins to be uses.
                                        Default = []
        overwrite_sg_pytest_ini (bool): if true overwrites
                                        the sg_pytest.ini parameter
                                        Default = True
        use_sg_pytest_ini (bool) :      if set to False it is
                                        not using the sg_pytest.ini parameters
                                        Default = False
        """
        # check params
        if params:
            self._params = params.copy()
        if 'plugins' not in self._params or self._params['plugins'] is None:
            self._params['plugins'] = []
        if 'src_dir' not in self._params or self._params['src_dir'] is None:
            self._params['src_dir'] = sg_const.DEFAULT_SRC_DIR
        if 'test_dir' not in self._params or self._params['test_dir'] is None:
            self._params['test_dir'] = sg_const.DEFAULT_TEST_DIR

        # read available plugins
        all_plugins = ''
        if os.path.isfile(sg_const.REQ_PLUGIN_PATH):
            with open(sg_const.REQ_PLUGIN_PATH) as file_handler:
                all_plugins = file_handler.read()
        all_plugins = all_plugins.replace(' ', '').split('\n')
        self._params['all_plugins'] = []
        for param in all_plugins:
            if param != '':
                self._params['all_plugins'].append(
                    param.split("=")[0].split(">")[0].split("<")[0].replace(
                        'pytest-',
                        '--'
                    )
                )

        # read sg_pytest_ini
        if use_sg_pytest_ini:
            config = configparser.ConfigParser()
            config.sections()
            config.read(sg_const.SG_PYTEST_INI)
            if 'pytest' in config:
                if 'plugins' in config['pytest']:
                    ini_plugins = config['pytest']['plugins'].split(" ")
                    self._params['plugins'] = list(set().union(
                        ini_plugins, self._params['plugins']))
                if not overwrite_sg_pytest_ini:
                    if 'test_dir' in config['pytest']:
                        self._params['test_dir'] = config['pytest']['test_dir']
                    if 'src_dir' in config['pytest']:
                        self._params['src_dir'] = config['pytest']['src_dir']

    def start_processing_test(self) -> dict:
        """Start the SGPytest tests and init the SGPytest Parameter.

        Returns:
            {
                "success_full",
                "command",
                "exit_code",
                "std_out",
                "std_err",
                "log_msg",
                "params"
            }
            or
            False if this sg_pytest is already processing

        """
        if self._processing:  # pragma: no cover
            return False
        output_str = "Start processing SG_Pytest - started"

        self._set_status(output_str, True)
        output_str = "Start processing SG_Pytest"
        return_struct = self._create_return_struct(log_msg=output_str)
        output_str = "Start processing SG_Pytest - finished"
        self._set_status(output_str, False, return_struct=return_struct)
        return return_struct

    def create_the_test_env(self, use_new_env: bool = False) -> dict:
        """Create the Test Environment.

        params:
            use_new_env (bool)  If True, the existing will be deleted
                                and a new one will be created
                                Default False

        Returns:
            {
                "success_full",
                "command",
                "exit_code",
                "std_out",
                "std_err",
                "log_msg",
                "params"
            }
            or
            False if this sg_pytest is already processing

        """
        if self._processing:  # pragma: no cover
            return False
        output_str = "Create Test Environment - started"
        self._set_status(output_str, True)
        call = None
        output_str = "Create Test Environment (%s)" % \
            (not self._use_existing_working_dir(use_new_env))
        if not self._use_existing_working_dir(use_new_env):
            call = "sh %s/%s %s %s" % \
                (
                    sg_const.APP_DIRECTORY,
                    sg_const.SHELL_SCRIPT_SG_CREATE_TEST_ENV,
                    sg_const.DATA_DIRECTORY,
                    self.test_run_dir
                )
        return_struct = self._create_return_struct(
            log_msg=output_str,
            command_to_call=call
        )
        output_str = "Create Test Environment - finished"
        self._set_status(output_str, False, return_struct=return_struct)
        return return_struct

    def execute_pytest(self) -> dict:
        """Run the sg_pytest.

        Returns:
            {
                "success_full",
                "command","exit_code",
                "std_out","std_err",
                "log_msg", "params"
            }
            or
            False if this sg_pytest is already processing

        """
        if self._processing:  # pragma: no cover
            return False
        output_str = "Run the sg_pytest - started"
        self._set_status(output_str, True)
        output_str = "Run the sg_pytest"
        call = "sh %s%s %s %s" % \
            (
                sg_const.APP_DIRECTORY,
                sg_const.SHELL_SCRIPT_SG_PYTEST,
                self.test_run_dir,
                self._get_pytest_param()
            )
        return_struct = self._create_return_struct(
            log_msg=output_str,
            command_to_call=call
        )
        tmp_out = str(return_struct['exit_code'][0])
        if return_struct['exit_code'][0] in sg_const.PYTEST_EXIT_CODE:
            tmp_out = sg_const.PYTEST_EXIT_CODE[return_struct['exit_code'][0]]
        if tmp_out:
            output_str += " - Result: " + tmp_out
            exit_code = return_struct['exit_code'][0]
            if exit_code == 99:
                return_struct['success_full'] = False
            elif exit_code not in sg_const.PYTEST_EXIT_CODE:
                return_struct['success_full'] = False
            else:
                return_struct['success_full'] = True
            return_struct['log_msg'][0] = output_str
        output_str = "Run the sg_pytest - finished"
        self._set_status(output_str, False, return_struct=return_struct)
        return return_struct

    @property
    def params(self):
        """Return the params."""
        return self._params

    @property
    def serial(self):
        """Return the serial."""
        return self._serial

    @property
    def last_return_struct(self):
        """Return the last return result."""
        return self._last_return_struct

    @property
    def status(self):
        """Return the status."""
        return self._status

    @property
    def processing(self):
        """Return if processing."""
        return self._processing

    @property
    def test_run_dir(self):
        """Return test run directory."""
        return self._test_run_dir

    @property
    def pytest_result_web_link(self):
        """Return test result link."""
        return self._pytest_result_web_link

    @property
    def pytest_cov_result_web_link(self):
        """Return coverage link."""
        return self._pytest_cov_result_web_link

    @property
    def static_dir(self):
        """Return static directory."""
        return self._static_dir

    def _set_status(
            self, status: str,
            processing: bool,
            return_struct: dict = None
    ):
        """Set the current status and dumps it."""
        self._status = status
        self._processing = processing
        if return_struct:
            self._last_return_struct = return_struct
        SGPytest._dump(self, self.serial)

    def _get_pytest_param(self) -> list:
        """Return all the parameter for pytest."""
        arg_dir = [
            "--rootdir=" + self.test_run_dir,
            self._params['test_dir'],
            self._params['src_dir']
        ]
        arg_html = ["--html=" + self.static_dir +
                    "/" + sg_const.PYTEST_WEB_SITE]
        arg_cov = [
            "--cov=" + self._params['src_dir'],
            "--cov-report=html:" + self.static_dir +
            sg_const.PYTEST_COV_WEB_DIR
        ]

        return_codes = arg_dir + arg_html + self._params['plugins'] + arg_cov
        return " ".join(str(x) for x in return_codes)

    def _use_existing_working_dir(self, use_new_env):
        """Check if working directories are available."""
        return use_new_env is False \
            and os.path.isdir(self.test_run_dir)

    def _create_return_struct(
            self,
            success_full: bool = True,
            command: str = '',
            exit_code: int = 0,
            std_out: str = '',
            std_err: str = '',
            log_msg: str = '',
            return_struct: dict = None,
            command_to_call: str = None
    ) -> dict:
        """Create the return struct."""
        if command_to_call is not None:
            is_all_ok, return_struct = SGPytest._check_output(
                command_to_call)
            return self._create_return_struct(
                log_msg=log_msg,
                success_full=is_all_ok,
                return_struct=return_struct
            )
        if return_struct is not None:
            return {
                "success_full": success_full,
                "command": [return_struct["command"]],
                "exit_code": [return_struct["exit_code"]],
                "std_out": [return_struct["std_out"]],
                "std_err": [return_struct["std_err"]],
                "log_msg": [log_msg],
                "params": self._params
            }
        return {
            "success_full": success_full,
            "command": [command],
            "exit_code": [exit_code],
            "std_out": [std_out],
            "std_err": [std_err],
            "log_msg": [log_msg],
            "params": self._params
        }

    @staticmethod
    def _check_output(command):
        """Run the command and return code, std_out and std_err."""
        process = subprocess.run(
            [command],
            shell=True,
            universal_newlines=True,
            stdout=subprocess.PIPE,  # --> only needed before python 3.7
            stderr=subprocess.PIPE  # --> only needed before python 3.7
            # capture_output=True --> only valide for python 3.7
        )
        return_struct = {
            "command": command,
            "exit_code": process.returncode,
            "std_out": process.stdout,
            "std_err": process.stderr
        }
        # --> only needed before python 3.7
        if not return_struct['std_out']:  # pragma: no cover
            return_struct['std_out'] = ''
        if not return_struct['std_err']:  # pragma: no cover
            return_struct['std_err'] = ''
        if process.returncode == 0:
            return True, return_struct
        return False, return_struct
