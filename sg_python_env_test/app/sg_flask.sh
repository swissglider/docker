#!/usr/bin/env bash

pip install --editable .
pip install pycodestyle==2.3.1
pip install flask     
pip install pytest-flake8
pip install pytest-docstyle
pip install pytest-pep8
pip install pytest-pylint
pip install pytest-pep257
pip install pytest-codestyle
pip install pytest
pip install pytest-cov
pip install pytest-html
sg_flask
sh sg_clean.sh
