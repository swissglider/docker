#!/usr/bin/env bash

find . -type d -name __pycache__ -exec rm -r {} \+
find . -type d -name .pytest_cache -exec rm -r {} \+
find . -type d -name *egg-info* -exec rm -r {} \+
find . -type d -name .eggs -exec rm -r {} \+
find . -type f -name ".coverage*" -exec rm -r {} \+
rm -rf src/sg_pytest/webapp/static/assets
rm -rf src/sg_pytest/webapp/static/cov
rm -rf src/sg_pytest/webapp/static/test.html
rm -rf src/sg_pytest/webapp/static/temp_static
rm -rf /tmp/*
rm -rf /static
rm -rf /test_run