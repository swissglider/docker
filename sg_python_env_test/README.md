# Python test Environment

@Swissglider  

## short description

This image creates the base image for the Pythontest Environment
Class SGPytest for run tests against Source Code and runs unittest

## how to install

```sh
docker build -t dev https://bitbucket.org/swissglider/docker/raw/master/sg_python_env_test/Dockerfile
docker run -d --name dev_c -p 8888:8888 dev

docker exec -it dev_c bash
docker exec -it dev_c tail -f /tmp/uwsgi.log

docker rm -f dev_c
docker rmi dev
```

```sh
docker build -t dev https://bitbucket.org/swissglider/docker/raw/master/sg_python_env_test/Dockerfile
docker run -d --name dev_c -p 8888:8888 -v /home/diener/developer/docker/sg_python_env_test/app:/data dev

docker run -d --name dev_c -p 8888:8888 -v /home/diener/developer/docker/sg_python_env_test/app:/data -e FLASK_APP=main.py -e FLASK_DEBUG=1 dev bash -c "while true ; do sleep 10 ; done"
docker exec -it dev_c bash
flask run --host=0.0.0.0 --port=8888

docker rm -f dev_c
docker rmi dev
```

## Install it for test in test docker python

```sh
docker run -it --rm --name dev_a -e TZ=Europe/Zurich -p 80:80 -v /home/diener/developer/docker/sg_python_env_test/app:/data -v /home/diener/developer/docker/sg_python_env_test/app:/app -w /data python sh sg_flask.sh

docker run -it --rm --name dev_a -e TZ=Europe/Zurich -p 80:80 -v /home/diener/developer/docker/sg_python_env_test/app:/data -v /home/diener/developer/docker/sg_python_env_test/app:/app -w /data python sh sg_pytest.sh

docker run -it --rm --name dev_a -e TZ=Europe/Zurich -p 80:80 -v /home/diener/developer/docker/sg_python_env_test/app:/data -v /home/diener/developer/docker/sg_python_env_test/app:/app -w /data python sh clean_env.sh

docker run -it --rm --name dev_a -e TZ=Europe/Zurich -p 80:80 -v /home/diener/developer/docker/sg_python_env_test/app:/data -v /home/diener/developer/docker/sg_python_env_test/app:/app -w /data python bash

sh sg_new.sh


 pytest testing src --html=/data/src/sg_pytest/webapp/static/test.html --cov=src --cov-report=html:/data/src/sg_pytest/webapp/static/cov

```

```sh
docker run -it --rm --name dev_a -e TZ=Europe/Zurich -p 80:80 -v /home/diener/developer/docker/sg_python_env_test/app:/data -v /home/diener/developer/docker/sg_python_env_test/app:/app -w /data python bash
pip install pytest
pip install pytest-cov
pip install pytest-html
pip install -e .
pytest --html=src/sg_pytest/webapp/static/test.html --cov --cov-report=html:src/sg_pytest/webapp/static/cov/ -s -vv
```

```sh
docker run -it --rm --name dev_a -e TZ=Europe/Zurich -p 80:80 -v /home/diener/developer/docker/sg_python_env_test/app:/data -v /home/diener/developer/docker/sg_python_env_test/app:/app -w /data python bash
python setup.py test
sh sg_clean.sh
```

## Installation

## Directory architecture - Volumes

- code
  - setup.py
  - setup.cfg
  - README.md
  - src (source Directory)
  - test (test files)

- tmp_home
  - .pypirc

## How to use it test framework

## How to build

[see howto Github build pypi](https://www.codementor.io/arpitbhayani/host-your-python-package-using-github-on-pypi-du107t7ku)

## Configuration

### setup.py

```python
from distutils.core import setup

setup(
    name = 'my_python_package',
    packages = ['my_python_package'],
    package_dir={'mymy_python_packagepkg': 'src/my_python_package'},
    version = 'version number',  # Ideally should be same as your GitHub release tag varsion
    description = 'description',
    author = '',
    author_email = '',
    url = 'https://github.com/<user>/<archive>',
    download_url = 'https://github.com/<user>/<archive>/archive/master.zip',
    keywords = ['tag1', 'tag2'],
    install_requires=[],
    classifiers = [],
    setup_requires=['pytest-runner', 'pylint'],
    tests_require=['pytest', 'pytest-asyncio'],
)
```

### setup.cfg

```cfg
[metadata]
description-file = README.md
[aliases]
test=pytest
```

### .pypirc

```cfg
[distutils]
index-servers =
  pypi
  pypitest

[pypi]
repository: https://pypi.python.org/pypi
username: YOUR_USERNAME_HERE
password: YOUR_PASSWORD_HERE

[pypitest]
repository: https://testpypi.python.org/pypi
username: YOUR_USERNAME_HERE
password: YOUR_PASSWORD_HERE
```

## Usage

```shell
pip install -e .
sg_pytest --cov --flake8 --cov --docstyle --codestyle --pep8 --pylint --pep257
sg_pytest --cov --flake8 --cov --docstyle --codestyle --pep8 --pylint --pep257 src/sg_pytest/sg_pytest.py testing/sg_pytest_test.py
```

## Open

- pythen implementation of "pip install -e ."
- make pypi
- implement in docker
